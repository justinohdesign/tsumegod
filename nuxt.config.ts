// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: [
    'nuxt-icon',
    '@vueuse/nuxt',
    '@nuxt/image-edge',
    '@nuxtjs/supabase',
    '@nuxtjs/color-mode',
    '@nuxtjs/tailwindcss',
    'vite-plugin-vue-type-imports/nuxt',
  ],
  css: ['assets/css/main.css'],
  colorMode: {
    dataValue: 'theme',
    classSuffix: '',
    preference: 'dark',
  },
});
