import type { Config } from 'tailwindcss';
import defaultTheme from 'tailwindcss/defaultTheme';

export default <Partial<Config>> {
  theme: {
    extend: {
      fontFamily: {
        sans: ['montserrat', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        'mine-shaft': {
          50: '#f7f7f7',
          100: '#e3e3e3',
          200: '#c8c8c8',
          300: '#a4a4a4',
          400: '#818181',
          500: '#666666',
          600: '#515151',
          700: '#434343',
          800: '#383838',
          900: '#282828',
          950: '#1a1a1a',
        },
        white: 'rgb(var(--white) / <alpha-value>)',
        red: 'rgb(var(--red) / <alpha-value>)',
        green: 'rgb(var(--green) / <alpha-value>)',
      },
      boxShadow: {
        main: '0 0 40px 0 rgba(0,0,0,0.25)',
      },
      maxWidth: {
        main: '1440px',
        editor: '900px',
        tsumego: '1140px',
      },
      gridTemplateColumns: {
        main: '0.3fr 0.5fr 0.3fr',
        editor: '0.4fr 0.6fr',
      },
    },
  },
};
