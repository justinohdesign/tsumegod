import { serverSupabaseClient, serverSupabaseUser } from '#supabase/server';
import { Database } from '~~/server/model/database';

export default defineEventHandler(async (event) => {
  const user = await serverSupabaseUser(event);

  if (user === null) {
    return {
      result: [],
    };
  }

  const client = serverSupabaseClient<Database>(event);

  const { data } = await client.from('tsumegos').select().eq('owner', user.id);

  if (data === null) {
    return {
      result: [],
    };
  }

  const tsumegos = await Promise.all(
    data.map(async (tsumego) => {
      const { data } = await client.storage
        .from('tsumegos')
        .download(`${tsumego.owner}/${tsumego.id}`);

      if (data === null) throw createError({ statusCode: 500, statusMessage: 'Unexpected' });

      return {
        ...tsumego,
        sgf: await data.text(),
      };
    }),
  );

  return {
    result: tsumegos,
  };
});
