import { serverSupabaseClient, serverSupabaseUser } from '#supabase/server';
import { Database } from '~~/server/model/database';

export default defineEventHandler(async (event) => {
  const user = await serverSupabaseUser(event);
  const client = serverSupabaseClient<Database>(event);
  const { id } = getQuery(event);

  const query = client
    .from('profiles')
    .select('*, stats:user_stats ( tsumegoId, solves )');

  if (id === undefined) {
    if (user === null) throw createError({ statusCode: 401, statusMessage: 'unauthoriszed' });
    query.eq('id', user.id);
  } else query.eq('id', id);

  const { data } = await query.single();

  if (data === null) return { result: null };

  return {
    result: {
      ...data,
      stats: data.stats === null ? [] : [data.stats].flat(),
    },
  };
});
