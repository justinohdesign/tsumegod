import { serverSupabaseClient, serverSupabaseUser } from '#supabase/server';
import { Database } from '~/server/model/database';

export default defineEventHandler(async (event) => {
  const id = Number(event.context.params?.id);
  const client = serverSupabaseClient<Database>(event);
  const user = await serverSupabaseUser(event);

  if (Number.isNaN(id)) { throw new TypeError('unexpected'); }

  const { data: tsumego } = await client
    .from('tsumegos')
    .select(
      `id, created_at, modified_at, name, public,
      stats:tsumego_stats (
        views,
        solves,
        attempts 
      ),
      votes:tsumego_votes (
        user_id,
        upvote
      ),
      owner:profiles!tsumegos_owner_fkey (
        id,
        username
      )`,
    )
    .eq('id', id)
    .single();

  if (
    tsumego === null ||
    tsumego.stats === null ||
    tsumego.owner === null ||
    tsumego.votes === null ||
    Array.isArray(tsumego.owner) ||
    Array.isArray(tsumego.stats)
  ) {
    return {
      result: null,
    };
  }

  const processedVotes = [tsumego.votes].flat().reduce<{
    upvote: number;
    downvote: number;
    currentUser?: boolean;
  }>((result, current) => {
    if (current.upvote) {
      result.upvote += 1;
    } else {
      result.downvote += 1;
    }

    if (current.user_id === user?.id) result.currentUser = current.upvote;

    return result;
  }, {
    upvote: 0,
    downvote: 0,
  });

  const { data } = await client.storage
    .from('tsumegos')
    .download(`/${tsumego.owner.id}/${tsumego.id}`);

  if (data === null) throw createError({ statusCode: 500, statusMessage: 'Unexpected' });

  const sgf = await data.text();

  return {
    result: {
      ...tsumego,
      owner: tsumego.owner,
      stats: tsumego.stats,
      votes: processedVotes,
      sgf,
    },
  };
});
