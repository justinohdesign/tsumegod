import { SgfWriterService, createBoardState } from '@go158/go-validator';
import { serverSupabaseClient, serverSupabaseUser } from '#supabase/server';
import { Database } from '~~/server/model/database';

interface RequestBody {
  sgf?: string;
  public?: boolean;
  name?: string;
}

export default defineEventHandler(async (event) => {
  const id = Number(event.context.params?.id);
  const body = await readBody<RequestBody>(event);
  const user = await serverSupabaseUser(event);
  const client = serverSupabaseClient<Database>(event);

  if (Number.isNaN(id) || user === null) throw createError({ statusCode: 400, statusMessage: 'Bad request' });

  if (body.public !== undefined) {
    // make sure a public tsumego has a valid solution and/or is not empty
    // TODO: much better filtering system
    if (body.public && body.sgf !== undefined && (!body.sgf.includes('CO[]') || body.sgf === SgfWriterService.fromBoardState(createBoardState()))) {
      throw createError({ statusCode: 400, statusMessage: 'Tsumego does not have a valid solution or is empty' });
    } else if (body.public) {
      // Get sgf from storage
      const { data } = await client.storage.from('tsumegos').download(`/${user.id}/${id}`);
      const sgf = await data?.text();
      if (sgf !== undefined && (!sgf.includes('CO[]') || sgf === SgfWriterService.fromBoardState(createBoardState()))) {
        throw createError({ statusCode: 400, statusMessage: 'Tsumego does not have a valid solution or is empty' });
      }
    }

    await client
      .from('tsumegos')
      .update({
        public: body.public,
      })
      .eq('id', id);
  }

  if (body.name !== undefined) {
    await client.from('tsumegos')
      .update({
        name: body.name,
      })
      .eq('id', id);
  }

  if (body.sgf !== undefined) {
    await client.storage.from('tsumegos').update(`/${user.id}/${id}`, body.sgf);
  }

  return {};
});
