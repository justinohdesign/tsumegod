import { serverSupabaseClient, serverSupabaseUser } from '#supabase/server';
import { Database } from '~~/server/model/database';

export default defineEventHandler(async (event) => {
  const id = Number(event.context.params?.id);
  const client = serverSupabaseClient<Database>(event);
  const user = await serverSupabaseUser(event);

  if (Number.isNaN(id) || user === null) throw createError({ statusCode: 400, statusMessage: 'Bad request' });

  await client.from('tsumegos').delete().eq('id', id);
  await client.storage.from('tsumegos').remove([`${user.id}/${id}`]);

  return {};
});
