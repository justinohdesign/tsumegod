import { serverSupabaseClient, serverSupabaseUser } from '#supabase/server';
import { Database } from '~~/server/model/database';

interface RequestBody {
  sgf: string;
}

export default defineEventHandler(async (event) => {
  const user = await serverSupabaseUser(event);

  if (user === null) {
    throw createError({ statusCode: 401, statusMessage: 'Unauthorized' });
  }

  const body = await readBody<RequestBody>(event);
  const client = serverSupabaseClient<Database>(event);

  const { data: tsumego } = await client
    .from('tsumegos')
    .insert({})
    .select()
    .single();

  if (tsumego === null) {
    return {
      result: null,
    };
  }

  await client.storage
    .from('tsumegos')
    .upload(`/${user.id}/${tsumego.id}`, body.sgf);

  return {
    result: {
      ...tsumego,
      sgf: body.sgf,
    },
  };
});
