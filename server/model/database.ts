export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[]

export interface Database {
  public: {
    Tables: {
      profiles: {
        Row: {
          created_at: string;
          id: string;
          modified_at: string;
          username: string;
        };
        Insert: {
          created_at?: string;
          id?: string;
          modified_at?: string;
          username: string;
        };
        Update: {
          created_at?: string;
          id?: string;
          modified_at?: string;
          username?: string;
        };
      };
      tsumego_stats: {
        Row: {
          attempts: number;
          id: number;
          solves: number;
          views: number;
        };
        Insert: {
          attempts?: number;
          id?: number;
          solves?: number;
          views?: number;
        };
        Update: {
          attempts?: number;
          id?: number;
          solves?: number;
          views?: number;
        };
      };
      tsumego_votes: {
        Row: {
          id: number;
          upvote: boolean;
          user_id: string;
        };
        Insert: {
          id: number;
          upvote: boolean;
          user_id?: string;
        };
        Update: {
          id?: number;
          upvote?: boolean;
          user_id?: string;
        };
      };
      tsumegos: {
        Row: {
          created_at: string;
          id: number;
          modified_at: string;
          name: string | null;
          owner: string;
          public: boolean;
          random: unknown | null;
        };
        Insert: {
          created_at?: string;
          id?: number;
          modified_at?: string;
          name?: string | null;
          owner?: string;
          public?: boolean;
        };
        Update: {
          created_at?: string;
          id?: number;
          modified_at?: string;
          name?: string | null;
          owner?: string;
          public?: boolean;
        };
      };
      user_stats: {
        Row: {
          solves: number;
          tsumegoId: number;
          userId: string;
        };
        Insert: {
          solves?: number;
          tsumegoId: number;
          userId?: string;
        };
        Update: {
          solves?: number;
          tsumegoId?: number;
          userId?: string;
        };
      };
    };
    Views: {
      [_ in never]: never
    };
    Functions: {
      increment_attempts: {
        Args: {
          tsumegoid: number;
        };
        Returns: undefined;
      };
      increment_solves: {
        Args: {
          tsumegoid: number;
        };
        Returns: undefined;
      };
      increment_views: {
        Args: {
          tsumegoid: number;
        };
        Returns: undefined;
      };
      random: {
        Args: {
          '': unknown;
        };
        Returns: number;
      };
      toggle_tsumego_vote: {
        Args: {
          id: number;
          value: boolean;
        };
        Returns: undefined;
      };
    };
    Enums: {
      [_ in never]: never
    };
    CompositeTypes: {
      [_ in never]: never
    };
  };
}
