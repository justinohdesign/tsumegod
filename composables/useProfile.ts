import { Database } from '~/server/model/database';

export default () => {
  const user = useSupabaseUser();
  const client = useSupabaseClient<Database>();

  const { data: username, refresh } = useAsyncData(async () => {
    if (user.value === null) return null;
    const { data } = await client.from('profiles').select().eq('id', user.value.id).maybeSingle();
    return data?.username ?? null;
  });

  watch(user, () => refresh());

  return {
    username,
  };
};
