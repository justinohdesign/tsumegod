import { AppContext, render } from 'vue';
import Snackbar from '~~/components/snackbar.vue';

export enum SnackBarType {
  SUCCESS,
  ERROR,
}

const useSnackBar = (
  appContext: AppContext,
  message: string,
  type: SnackBarType,
  duration = 3,
) => {
  const node = h(Snackbar, {
    duration,
    message,
    type,
    onDestroy: () => {
      close();
    },
  });

  node.appContext = appContext;

  const div = document.createElement('div');
  render(node, div);

  if (div.firstElementChild) {
    document.body.appendChild(div.firstElementChild);
  }

  const close = () => {
    render(null, div);
  };

  return {
    close,
  };
};

export default useSnackBar;
