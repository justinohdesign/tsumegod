export default () => {
  const { data: tsumegos, pending } = useFetch('/api/me/tsumegos', {
    transform: data => data.result,
  });

  const selectedIndex = ref<number>();

  const selected = computed(() => {
    if (selectedIndex.value === undefined) return undefined;
    return tsumegos.value?.at(selectedIndex.value);
  });

  const save = () => {};

  const download = () => {};

  const remove = () => {};

  const select = () => {};

  const create = () => {};

  return {
    tsumegos,
    selected,
    loading: pending,
    save,
    download,
    delete: remove,
    select,
    create,
  };
};
