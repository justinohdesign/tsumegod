import { Database } from '~~/server/model/database';

export default (id?: number) => {
  const client = useSupabaseClient<Database>();

  const {
    data: tsumego,
    pending: loading,
    refresh,
  } = useFetch(`/api/tsumego${Number.isNaN(id) || id === undefined ? '' : `/${id}`}`, {
    transform: data => data.result ?? null,
  });

  const incrementViews = async () => {
    if (tsumego.value === null) return;

    // This visually changes the client (does not update the server)
    tsumego.value.stats.views += 1;

    await client.rpc('increment_views', {
      tsumegoid: tsumego.value.id,
    });
  };

  const increaseAttemps = async () => {
    if (tsumego.value === null) return;

    // This visually changes the client (does not update the server)
    tsumego.value.stats.attempts += 1;

    await client.rpc('increment_attempts', {
      tsumegoid: tsumego.value.id,
    });
  };

  const increaseSolves = async () => {
    if (tsumego.value === null) return;

    // This visually changes the client (does not update the server)
    tsumego.value.stats.solves += 1;

    await client.rpc('increment_solves', {
      tsumegoid: tsumego.value.id,
    });
  };

  const updateVote = async (value: boolean) => {
    if (tsumego.value === null) return;

    await client.rpc('toggle_tsumego_vote', {
      id: tsumego.value.id,
      value,
    });

    if (value) {
      if (tsumego.value.votes.currentUser === false) tsumego.value.votes.downvote -= 1;
      tsumego.value.votes.upvote += tsumego.value.votes.currentUser === true ? -1 : +1;

      if (tsumego.value.votes.currentUser === undefined) tsumego.value.votes.currentUser = true;
      else if (!tsumego.value.votes.currentUser) tsumego.value.votes.currentUser = true;
      else tsumego.value.votes.currentUser = undefined;
    } else {
      if (tsumego.value.votes.currentUser === true) tsumego.value.votes.upvote -= 1;
      tsumego.value.votes.downvote += tsumego.value.votes.currentUser === false ? -1 : +1;

      if (tsumego.value.votes.currentUser === undefined) tsumego.value.votes.currentUser = false;
      else if (tsumego.value.votes.currentUser) tsumego.value.votes.currentUser = false;
      else tsumego.value.votes.currentUser = undefined;
    }
  };

  const download = () => {
    if (tsumego.value === null) return;
    const blob = new Blob([tsumego.value.sgf], { type: 'plain/text' });
    const downloadLink = document.createElement('a');
    downloadLink.href = URL.createObjectURL(blob);
    downloadLink.download = `${tsumego.value.id}.sgf`;
    downloadLink.click();
  };

  return {
    loading,
    tsumego,
    download,
    updateVote,
    refresh,
    increaseAttemps,
    increaseSolves,
    incrementViews,
  };
};
