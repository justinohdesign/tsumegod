import { BoardState, Position, useBoardState } from '@go158/go-validator';

export interface TreeEmits {
  (event: 'nodeClicked', node: BoardState): void;
}

const SIZE = 100;
const PADDING = 20;

const useTree = (
  initial: Ref<BoardState>,
  current: Ref<BoardState>,
  emits: TreeEmits,
) => {
  const { x, y } = useMouse();
  let swipeAnchorPoint: { x: number; y: number };
  let previousViewBox = {
    x: -SIZE / 2,
    y: -SIZE / 4,
    width: SIZE,
    height: SIZE,
  };

  const getLines = (
    from: Position,
    to: Position,
    current = false,
  ): Array<SVGLineElement> => {
    const result = new Array<SVGLineElement>();

    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');

    if (to.col - from.col > 1) {
      const offsetLine = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      );
      offsetLine.setAttribute('x1', `${from.col * 12}`);
      offsetLine.setAttribute('y1', `${from.row * 12}`);
      offsetLine.setAttribute('x2', `${(to.col - 1) * 12}`);
      offsetLine.setAttribute('y2', `${from.row * 12}`);
      offsetLine.setAttribute('stroke', '#606060');
      offsetLine.setAttribute('stroke-width', '0.4');
      offsetLine.setAttribute('stroke-linecap', 'round');
      result.push(offsetLine);
      line.setAttribute('x1', `${(to.col - 1) * 12}`);
    } else {
      line.setAttribute('x1', `${from.col * 12}`);
    }

    let offsetX = 0;
    let offsetY = current ? 3 : 2;

    if (to.col > from.col) {
      offsetX = current ? 1.5 * Math.sqrt(2) : Math.sqrt(2);
      offsetY = current ? 1.5 * Math.sqrt(2) : Math.sqrt(2);
    }

    line.setAttribute('y1', `${from.row * 12}`);
    line.setAttribute('x2', `${to.col * 12 - offsetX}`);
    line.setAttribute('y2', `${to.row * 12 - offsetY}`);
    line.setAttribute('stroke', '#606060');
    line.setAttribute('stroke-width', '0.4');

    result.push(line);

    return result;
  };

  const numberOfChildBranches = (node: Readonly<BoardState>): number => {
    let result = 0;

    Array.from(node.children.values()).forEach((child) => {
      result += numberOfChildBranches(child);
    });

    return Math.max(0, node.children.size - 1) + result;
  };

  const createNodeSvg = (
    node: BoardState,
    currentPosition: Position = { row: 0, col: 0 },
    previousPosition: Position | null = null,
    parentInMainLine = false,
  ): {
    svg: SVGSVGElement;
    inMainLine: boolean;
    currentBoardStateNodePosition: Position | undefined;
  } => {
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');

    let previousGameTreeChildBranches = 0;
    let currentNodeInMainLine = node === current.value;
    let currentBoardStatePos: Position | undefined;
    let positionOfNextBoardStateInLine: Position | undefined;
    let mainLineLines: Array<SVGElement> | undefined;
    Array.from(node.children.values()).forEach((child) => {
      const childPosition = {
        row: currentPosition.row + 1,
        col: currentPosition.col + previousGameTreeChildBranches,
      };

      const childIsNextBoardStateInLine =
        useBoardState(child).key.value === node.nextBoardStateKeyInLine;

      const {
        svg: childNodeSvg,
        inMainLine,
        currentBoardStateNodePosition,
      } = createNodeSvg(
        child,
        childPosition,
        currentPosition,
        parentInMainLine
          ? childIsNextBoardStateInLine
          : node === current.value && childIsNextBoardStateInLine,
      );

      if (inMainLine) {
        currentNodeInMainLine = inMainLine;
        mainLineLines = getLines(
          currentPosition,
          childPosition,
          child === current.value,
        );
      } else if (childIsNextBoardStateInLine && node === current.value) {
        positionOfNextBoardStateInLine = childPosition;
      }

      if (currentBoardStateNodePosition) {
        currentBoardStatePos = currentBoardStateNodePosition;
      }

      svg.append(...childNodeSvg.childNodes);

      previousGameTreeChildBranches +=
        numberOfChildBranches(child as BoardState) + 1;
    });

    if (mainLineLines !== undefined) {
      mainLineLines.forEach((line) => {
        line.setAttribute('stroke', 'white');
        svg.appendChild(line);
      });
    } else if (positionOfNextBoardStateInLine) {
      getLines(currentPosition, positionOfNextBoardStateInLine).forEach(
        (line) => {
          line.setAttribute('stroke', 'white');
          svg.appendChild(line);
        },
      );
    }

    if (previousPosition !== null) {
      getLines(previousPosition, currentPosition).forEach((line) => {
        line.setAttribute(
          'stroke',
          currentNodeInMainLine || parentInMainLine ? 'white' : 'white',
        );
        if (!currentNodeInMainLine) line.setAttribute('stroke-opacity', '0.1');
        svg.appendChild(line);
      });
    }

    const circle = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'circle',
    );
    circle.setAttribute('cx', `${currentPosition.col * 12}`);
    circle.setAttribute('cy', `${currentPosition.row * 12}`);
    circle.setAttribute('r', current.value === node ? '3' : '2');
    circle.setAttribute('style', 'cursor: pointer;');
    circle.setAttribute(
      'fill',
      node.correctSolution
        ? '#3AB82F'
        : node.comment.length > 0
          ? '#DBB73A'
          : currentNodeInMainLine || parentInMainLine
            ? 'white'
            : '#606060',
    );

    if (current.value === node) {
      currentBoardStatePos = {
        row: currentPosition.row * 12,
        col: currentPosition.col * 12,
      };
    }

    useEventListener(circle, 'click', () => emits('nodeClicked', node));

    svg.appendChild(circle);

    return {
      svg,
      inMainLine: currentNodeInMainLine,
      currentBoardStateNodePosition: currentBoardStatePos,
    };
  };

  const createAnimate = (currentNodePosition: Position): SVGElement => {
    let offsetX = previousViewBox.x;
    let offsetY = previousViewBox.y;

    const currentX = currentNodePosition.col;
    const currentY = currentNodePosition.row;

    const padding = (PADDING * previousViewBox.width) / 100;
    const size = (SIZE * previousViewBox.width) / 100;

    if (currentY - offsetY < padding) {
      offsetY -= padding - (currentY - offsetY);
    }

    if (currentY - offsetY > size - padding) {
      offsetY += currentY - offsetY - size + padding;
    }

    if (currentX - offsetX < padding) {
      offsetX -= padding - (currentX - offsetX);
    }

    if (currentX - offsetX > size - padding) {
      offsetX += currentX - offsetX - size + padding;
    }

    const newViewBox = {
      x: offsetX,
      y: offsetY,
      width: previousViewBox.width,
      height: previousViewBox.height,
    };

    const animate = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'animate',
    );

    animate.setAttribute('attributeName', 'viewBox');
    animate.setAttribute('dur', '0.1s');
    animate.setAttribute(
      'from',
      `${previousViewBox.x} ${previousViewBox.y} ${previousViewBox.width} ${previousViewBox.height}`,
    );
    animate.setAttribute(
      'to',
      `${newViewBox.x} ${newViewBox.y} ${newViewBox.width} ${newViewBox.height}`,
    );

    previousViewBox = newViewBox;

    return animate;
  };

  // If initial board state changes, it means we have selected a new tsumego, we reset the previousViewBox
  watch(initial, () => {
    previousViewBox = {
      x: -SIZE / 2,
      y: -SIZE / 4,
      width: SIZE,
      height: SIZE,
    };
  });

  const svg = computed(() => {
    const { svg, currentBoardStateNodePosition } = createNodeSvg(initial.value);
    svg.setAttribute('width', '100%');
    svg.setAttribute('height', '100%');

    if (currentBoardStateNodePosition !== undefined) {
      const animate = createAnimate(currentBoardStateNodePosition);
      svg.appendChild(animate);
      svg.setAttribute('viewBox', animate.getAttribute('to')!);
    }

    return svg;
  });

  usePointerSwipe(svg as unknown as ComputedRef<HTMLElement>, {
    onSwipeStart: () => {
      const point = svg.value.createSVGPoint();
      point.x = x.value;
      point.y = y.value;
      swipeAnchorPoint = {
        x: point.matrixTransform(svg.value.getScreenCTM()?.inverse()).x,
        y: point.matrixTransform(svg.value.getScreenCTM()?.inverse()).y,
      };
    },
    onSwipe: () => {
      const point = svg.value.createSVGPoint();
      point.x = x.value;
      point.y = y.value;

      const targetPoint = point.matrixTransform(
        svg.value.getScreenCTM()?.inverse(),
      );

      const deltaX = swipeAnchorPoint.x - targetPoint.x;
      const deltaY = swipeAnchorPoint.y - targetPoint.y;

      svg.value.viewBox.baseVal.x += deltaX;
      svg.value.viewBox.baseVal.y += deltaY;

      previousViewBox.x = svg.value.viewBox.baseVal.x;
      previousViewBox.y = svg.value.viewBox.baseVal.y;
    },
    threshold: 0,
  });

  useEventListener(svg, 'wheel', (event: WheelEvent) => {
    event.preventDefault();

    const { deltaY } = event;

    svg.value.viewBox.baseVal.width = Math.min(
      200,
      Math.max(50, svg.value.viewBox.baseVal.width + deltaY * 0.05),
    );
    svg.value.viewBox.baseVal.height = Math.min(
      200,
      Math.max(50, svg.value.viewBox.baseVal.height + deltaY * 0.05),
    );

    previousViewBox.width = svg.value.viewBox.baseVal.width;
    previousViewBox.height = svg.value.viewBox.baseVal.height;
  });

  return {
    svg,
  };
};

export default useTree;
