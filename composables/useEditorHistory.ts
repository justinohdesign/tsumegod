export interface Command {
  execute: () => void;
  undo: () => void;
}

export default () => {
  const metaKey = useKeyModifier('Meta');
  const ctrlKey = useKeyModifier('Control');
  const shiftKey = useKeyModifier('Shift');

  const undoHistory = new Array<Command>();
  const redoHistory = new Array<Command>();

  const pushCommand = (command: Command) => {
    undoHistory.push(command);
    redoHistory.length = 0;
  };

  const undo = () => {
    const undoAction = undoHistory.pop();
    undoAction?.undo();
    undoAction !== undefined && redoHistory.push(undoAction);
  };

  const redo = () => {
    const redoAction = redoHistory.pop();
    redoAction?.execute();
    redoAction !== undefined && undoHistory.push(redoAction);
  };

  onKeyStroke('z', () => {
    if (metaKey.value || ctrlKey.value) {
      if (shiftKey.value) redo();
      else undo();
    }
  },
  );
  onKeyStroke('Z', () => (ctrlKey.value || metaKey.value) && redo());

  return {
    pushCommand,
    undo,
    redo,
  };
};
