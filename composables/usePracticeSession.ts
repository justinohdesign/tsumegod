export default () => {
  const solves = useState('practiceSessionSolves', () => 0);
  const total = useState('practiceSessionTotals', () => 0);

  return {
    solves,
    total,
  };
};
