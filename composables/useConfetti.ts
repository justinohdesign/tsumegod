import confetti from 'canvas-confetti';

export default () => {
  const { x, y } = useMouse();
  const { width, height } = useWindowSize();

  const fire = () => {
    confetti({
      origin: {
        x: x.value / width.value,
        y: y.value / height.value,
      },
      spread: 360,
      startVelocity: 20,
      particleCount: 25,
      ticks: 50,
    });
  };

  return {
    fire,
  };
};
